//! Parses the `package.metadata.skavos-bootimage` configuration table
//!
//! This file is inspired by [bootimage's config.rs](https://github.com/rust-osdev/bootimage/blob/master/src/config.rs)

use std::fs::File;
use std::io::Read;
use std::path::Path;

use anyhow::{anyhow, Result};
use toml::Value;

/// Represents the `package.metadata.skavos-bootimage` configuration table
#[derive(Debug, Default)]
#[non_exhaustive]
pub struct Config {
    /// The storage device for release profile
    pub run_storage: Option<String>,

    /// The mountpoint of the storage device for test profile
    ///
    /// The host will be read-only, as a fresh filesystem will be sent to the ghost.
    pub test_mountpoint: Option<String>,

    /// Additional arguments passed to the runner for test profile
    pub test_args: Vec<String>,

    /// An exit code that should be considered as success for test profile
    pub test_success_exit_code: i32,
}

impl Config {
    /// Reads the configuration from a `package.metadata.skavos-bootimage` in the Cargo.toml.
    pub fn read_config(project_path: &Path) -> Result<Self> {
        let manifest_path = project_path.join("Cargo.toml");

        let cargo_toml: Value = {
            let mut content = String::new();

            File::open(manifest_path)?.read_to_string(&mut content)?;

            content.parse::<Value>()?
        };

        let metadata = cargo_toml
            .get("package")
            .and_then(|table| table.get("metadata"))
            .and_then(|table| table.get("skavos-bootimage"));

        let metadata = match metadata {
            None => return Ok(Self::default()),

            Some(metadata) => metadata
                .as_table()
                .ok_or_else(|| anyhow!("Bootimage configuration invalid: {:?}", metadata))?,
        };

        let mut config = Self::default();

        for (key, value) in metadata {
            config.parse_config_entry(key.as_str(), value)?;
        }

        Ok(config)
    }

    fn parse_config_entry(&mut self, key: &str, value: &Value) -> Result<()> {
        match (key, value.clone()) {
            ("run-storage", Value::String(path)) => {
                self.run_storage = Some(path);
            },

            ("test-args", Value::Array(array)) => {
                self.test_args = parse_string_array(array, "test-args")?;
            },

            ("test-mountpoint", Value::String(path)) => {
                self.test_mountpoint = Some(path);
            },

            ("test-success-exit-code", Value::Integer(exit_code)) => {
                self.test_success_exit_code = exit_code as i32;
            },

            (key, value) => {
                return Err(anyhow!("unexpected key `{}` with value `{}` in `package.metadata.skavos-bootimage`", key, value));
            },
        }

        Ok(())
    }
}

fn parse_string_array(array: Vec<Value>, prop_name: &str) -> Result<Vec<String>> {
    let mut parsed = Vec::new();

    for value in array {
        match value {
            Value::String(s) => parsed.push(s),
            _ => return Err(anyhow!("{} must be a list of strings", prop_name)),
        }
    }

    Ok(parsed)
}
