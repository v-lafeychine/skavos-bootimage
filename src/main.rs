//! Executable for `skavos`.

mod config;

use std::path::{Path, PathBuf};
use std::process::{exit, Command};

use anyhow::{anyhow, Result};
use bootloader::UefiBoot;
use config::Config;

const STORAGE_TEST_SIZE: u64 = 40_000;

fn main() -> Result<()> {
    let mut args = std::env::args();
    args.next();

    let kernel = PathBuf::from(args.next().ok_or_else(|| anyhow!("There is no executable name"))?);
    let out_dir = kernel.parent().ok_or_else(|| anyhow!("There is no parent directory"))?;

    let uefi_path = out_dir.join("uefi");
    let is_test = out_dir.ends_with("deps");

    let project_path: &Path = {
        let env_manifest = std::env::var("CARGO_MANIFEST_DIR").map_err(|_| anyhow!("CARGO_MANIFEST_DIR not set"))?;

        &Path::new(&env_manifest).join("")
    };

    let config = Config::read_config(project_path)?;

    UefiBoot::new(&kernel).create_disk_image(&uefi_path)?;

    let mut qemu = Command::new("qemu-system-x86_64");
    let mut qemu_cmd = qemu
        .arg("-bios")
        .arg(ovmf_prebuilt::ovmf_pure_efi())
        .arg("-drive")
        .arg(format!("format=raw,file={}", uefi_path.display()));

    if !is_test {
        if let Some(storage) = config.run_storage {
            let storage_path = project_path.join(storage);

            if !storage_path.exists() {
                return Err(anyhow!("Storage file `{}` does not exist", storage_path.display()));
            }

            qemu_cmd = qemu_cmd.arg("-drive").arg(format!("format=raw,file={}", storage_path.display()));
        }
    }

    if is_test {
        if let Some(mountpoint) = config.test_mountpoint {
            let filesystem_path = out_dir.join("skavos-filesystem.ext2");
            let src_folder = project_path.join(mountpoint);

            let mut mke2fs = Command::new("mke2fs");
            let mke2fs_cmd = mke2fs
                .arg(filesystem_path.display().to_string())
                .arg(STORAGE_TEST_SIZE.to_string())
                .args(["-b", "1024"])
                .args(["-t", "ext2"])
                .args(["-d", &src_folder.display().to_string()])
                .args(["-E", "no_copy_xattrs"])
                .arg("-q")
                .args(["-F", "-F"]);

            if execute(mke2fs_cmd)? != 0 {
                return Err(anyhow!("mke2fs failed"));
            }

            qemu_cmd = qemu_cmd.arg("-drive").arg(format!("format=raw,file={}", filesystem_path.display()));
        };

        qemu_cmd = qemu_cmd.args(config.test_args);
    }

    let status = execute(qemu_cmd)?;

    if is_test {
        exit(if status == config.test_success_exit_code { 0 } else { 1 });
    }

    exit(status);
}

fn execute(cmd: &mut Command) -> Result<i32> {
    cmd.spawn()?.wait()?.code().ok_or_else(|| anyhow!("Unexpected result code"))
}
