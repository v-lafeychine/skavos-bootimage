# Bootimage

Creates a bootable disk image of a Rust OS kernel, inspired by the deprecated [`rust-osdev/bootimage`](https://github.com/rust-osdev/bootimage).

This project is not intended to be flexible: It only serves the [`skavos`](https://gitlab.crans.org/pigeonmoelleux/skavos) project.


## Installation

This wrapper is used as a `runner` of `cargo`:

```
cargo install --path .
```
